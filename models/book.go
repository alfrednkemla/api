package models

import "go.mongodb.org/mongo-driver/bson/primitive"

//Create Struct
type Book struct {
	ID     primitive.ObjectID `json:"_id,omitempty" bson:"_id,omitempty"`
	Isbn   string             `json:"isbn,omitempty" bson:"isbn,omitempty"`
	Title  string             `json:"title,omitempty" bson:"title,omitempty"`
	Author author             `json:"author,omitempty" bson:"author,omitempty"`
}

type author struct {
	FirstName string `json:"firstname,omitempty" bson:"firstname,omitempty"`
	LastName  string `json:"lastname,omitempty" bson:"lastname,omitempty"`
}

type BookFilter struct {
	Isbn   string `json:"isbn,omitempty" bson:"isbn,omitempty"`
	Title  string `json:"title,omitempty" bson:"title,omitempty"`
	AuthorFirstName string `json:"author.firstname,omitempty" bson:"author.firstname,omitempty"`
	AuthorLastName string `json:"author.lastname,omitempty" bson:"author.lastname,omitempty"`
}
