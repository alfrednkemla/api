package endPoints

import (
	"context"
	"encoding/json"
	"log"
	"net/http"

	"github.com/alfredn/api/helper"
	"github.com/alfredn/api/models"
)

/*
GetBooks return every book if the book filter is not set by the request body.
The Book Id is not use to filter the book since I want to return multiple record.
The first record found should be a record similar to the search.
*/
func GetBooks(w http.ResponseWriter, r *http.Request) {
	w.Header().Set("Content-Type", "application/json")

	// we created Book array
	var books []models.Book
	// we created a BookFilter
	var book models.BookFilter

	// we set the book filter
	json.NewDecoder(r.Body).Decode(&book)

	//Connection mongoDB with helper class
	collection := helper.ConnectDB()

	// We use the book filter to get books.
	cur, err := collection.Find(context.TODO(), book)

	if err != nil {
		helper.GetError(err, w)
		return
	}

	// Close the cursor once finished
	/*A defer statement defers the execution of a function until the surrounding function returns.
	simply, run cur.Close() process but after cur.Next() finished.*/
	defer cur.Close(context.TODO())

	for cur.Next(context.TODO()) {

		// create a value into which the single document can be decoded
		var book models.Book
		// & character returns the memory address of the following variable.
		err := cur.Decode(&book) // decode similar to deserialize process.
		if err != nil {
			log.Fatal(err)
		}

		// add item our array
		books = append(books, book)
	}

	if err := cur.Err(); err != nil {
		log.Fatal(err)
	}

	json.NewEncoder(w).Encode(books) // encode similar to serialize process.
}
